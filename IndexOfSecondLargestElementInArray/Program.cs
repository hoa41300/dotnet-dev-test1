﻿namespace IndexOfSecondLargestElementInArray;

public class Program
{
    static void Main(string[] args)
    {
        var myArr = new[] { 1, 2, 3 };
        var result = IndexOfSecondLargestElementInArray(myArr);
        Console.WriteLine(result);
    }

    public static int IndexOfSecondLargestElementInArray(int[] x)
    {
        if (x.Length <= 1)
        {
            return -1;
        }
        if (x.Length == 2)
        {
            int result = x.Min();
            return Array.IndexOf(x, result);
        }
        else
        {
            int largest = int.MinValue;
            int second = int.MinValue;
            foreach (int i in x)
            {
                if (i > largest)
                {
                    second = largest;
                    largest = i;
                }
                else if (i > second)
                {
                    second = i;
                }
            }

            int duplicateLargest = 0, duplicateSecond = 0;
            for (int i = 0; i < x.Length; i++)
            {
                if (x[i] == duplicateSecond)
                {
                    duplicateSecond++;
                }
                if (x[i] == duplicateLargest)
                {
                    duplicateLargest++;
                }
            }
            if (duplicateLargest != 0)
            {
                int count = 0;
                int index = 0;
                for (int i = 0; i < x.Length; i++)
                {
                    if (x[i] == largest)
                    {
                        count++;
                    }
                    if (count == 2)
                    {
                        index = i;
                    }
                }
                return index;
            }
            if (duplicateSecond != 0)
            {
                return Array.IndexOf(x, x.First(i => i == second));
            }

            return Array.IndexOf(x, second);

        }
    }
}