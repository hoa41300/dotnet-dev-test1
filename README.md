## hướng dẫn cách làm bài:
dùng visual studio mở file ```test-practice.sln``` và hoàn thành các bài tập.
mỗi 1 project sẽ có file README.md hướng dẫn chi tiết phải làm những công việc nào
thời gian làm bài sẽ là 60p
## lưu ý: source code phải luôn luôn build thành công nếu không sẽ không thể run tests được

## kiểm tra kết quả của mình bằng cách chạy unit tests
	trên thanh menu của Visual studio -> Test -> Test Explorer -> chạy test để kiểm tra kết quả.
##  happy coding <3


## how to add solution
```
dotnet sln add IndexOfSecondLargestElementInArray\IndexOfSecondLargestElementInArray.csproj --solution-folder Practices
dotnet sln add MixTwoStrings\MixTwoStrings.csproj --solution-folder Practices
dotnet sln add SortArrayDESC\SortArrayDESC.csproj --solution-folder Practices

dotnet sln add Tests\IndexOfSecondLargestElementInArray.Tests\IndexOfSecondLargestElementInArray.Tests.csproj --solution-folder Tests

dotnet sln add Tests\MixTwoStrings.Tests\MixTwoStrings.Tests.csproj --solution-folder Tests

dotnet sln add Tests\SortArrayDESC.Tests\SortArrayDESC.Tests.csproj --solution-folder Tests

```