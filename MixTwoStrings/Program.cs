﻿var result = PracticeStrings.MixTwoStrings("", "");
Console.WriteLine(result);
Console.WriteLine("hello world");

public class PracticeStrings
{

    public static string MixTwoStrings(string value1, string value2)
    {
        // xem file README.md
        if (value1.Length == 0 && value2.Length == 0)
        {
            return String.Empty;
        }
        if (value1.Length == 0 && value2.Length != 0)
        {
            return value2;
        }
        if (value2.Length == 0 && value1.Length != 0)
        {
            return value1;
        }
        if (value1.Length == value2.Length)
        {
            string newString = "";
            for (int i = 0; i < value1.Length; i++)
            {
                newString = newString + value1[i];
                newString = newString + value2[i];
            }
            return newString;
        }
        if (value1.Length != value2.Length)
        {
            string newString = "";
            if (value1.Length > value2.Length)
            {

                for (int i = 0; i < value2.Length; i++)
                {
                    newString = newString + value1[i];
                    newString = newString + value2[i];
                }
                for (int i = value2.Length; i < value1.Length; i++)
                {
                    newString += value1[i];
                }
                return newString;
            }
            else
            {
                for (int i = 0; i < value1.Length; i++)
                {
                    newString = newString + value1[i];
                    newString = newString + value2[i];
                }
                for (int i = value1.Length; i < value2.Length; i++)
                {
                    newString += value2[i];
                }
                return newString;
            }
        }
        return string.Empty;
    }
}

