﻿namespace SortArrayDESC;

public class Program
{
    static void Main(string[] args)
    {
        var sorted = SortArrayDesc(new int[] { 1 });
        Console.WriteLine(sorted);
    }

    public static int[] SortArrayDesc(int[] x)
    {
        //kết quả hàm sau khi thực thi là mảng số nguyên mới đã sắp xếp theo thứ tự giảm dần
        if (x == null || x.Length == 0)
        {
            return x;
        }
        if (x == Array.Empty<int>())
        {
            return x;
        }
        if (x.Length == 1)
        {
            return x;
        }

        for (int i = 0; i < x.Length; i++)
        {
            for (int j = i + 1; j < x.Length; j++)
            {
                if (x[i] < x[j])
                {
                    (x[i], x[j]) = (x[j], x[i]);
                }
            }
        }
        return x;

    }

}